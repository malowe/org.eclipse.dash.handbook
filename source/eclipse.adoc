////
 * Copyright (C) 2015,2022 Eclipse Foundation, Inc. and others. 
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-FileType: SOURCE
 *
 * SPDX-License-Identifier: EPL-2.0
////

include::config.adoc[]

:forgeName: Eclipse
:aforgeName: an Eclipse
:forgeMachineName: eclipse
:namespace: org.eclipse
:wwwUrl: https://www.eclipse.org
:forgeUrl: https://projects.eclipse.org
:downloadsHost: https://downloads.eclipse.org
:createUrl: https://projects.eclipse.org/node/add/project-proposal
:projectListUrl: http://projects.eclipse.org/list-of-projects
:bugzillaUrl: https://bugs.eclipse.org/bugs
:gitUrl: https://git.eclipse.org/c
:gitlabUrl: https://gitlab.eclipse.org
:gerritUrl: https://git.eclipse.org/r
:gitHubUrl: https://github.com/eclipse
:forumsUrl: http://www.eclipse.org/forums
:mailingListsUrl: http://www.eclipse.org/mail/

:pmiSampleImage: images/pmi-egit25.png

= Eclipse Foundation Project Handbook

include::chapters/notices.adoc[]

include::chapters/preamble.adoc[]

include::chapters/starting.adoc[]

include::chapters/resources.adoc[]

include::chapters/security.adoc[leveloffset=+1]

include::chapters/contributing.adoc[leveloffset=+1]

include::chapters/elections.adoc[]

include::chapters/paperwork.adoc[]

include::chapters/specifications.adoc[leveloffset=+1]

include::chapters/tlp.adoc[leveloffset=+1]

include::chapters/ip.adoc[]

include::chapters/legaldoc.adoc[leveloffset=+1]

include::chapters/release.adoc[]

include::chapters/pmi.adoc[]

include::chapters/trademarks.adoc[]

include::chapters/promotion.adoc[leveloffset=+1]

include::chapters/community.adoc[]

include::chapters/checklist.adoc[leveloffset=+1]

// include::chapters/links.adoc[]

include::chapters/glossary.adoc[]

include::chapters/contact.adoc[]

= Appendices

include::chapters/legaldoc-plugins.adoc[leveloffset=+1]

include::efdp/source/development_process.adoc[leveloffset=+1]

include::chapters/dpia.adoc[leveloffset=+1]