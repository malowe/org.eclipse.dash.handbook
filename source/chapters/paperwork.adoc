////
 * Copyright (C) 2015 Eclipse Foundation, Inc. and others. 
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-FileType: SOURCE
 *
 * SPDX-FileCopyrightText: 2015 Eclipse Foundation, Inc.
 * SPDX-FileCopyrightText: 2015 Contributors to the Eclipse Foundation
 *
 * SPDX-License-Identifier: EPL-2.0
////

[[paperwork]]
== Committer Paperwork

The Eclipse Foundation needs to ensure that all committers with write access to the code, websites, and issue tracking systems understand their role in the intellectual property process. The Eclipse Foundation also needs to ensure that we have accurate records of the people who are acting as change agents on the projects. To ensure that committers understand their role, and that the Eclipse Foundation has accurate records, committers must provide documentation asserting that they have read, understood, and will follow the committer guidelines. Committers must also gain their employers consent to their participation in Eclipse Foundation open source projects.

All committers must complete the _Committer Agreement Workflow_, which includes the _Committer Questionnaire_ and documentation as described below.

[[paperwork-questionnaire]]
=== Committer Questionnaire

The _Committer Questionnaire_ is an online form that must be completed by all new committers. This form offers two separate paths: one for committers who work for a member company that has provided a signed {mccaUrl}[Member Committer and Contributor Agreement] (MCCA), and one for everyone else.

.The Committer Agreement Workflow starts by asking for employment status.
image::images/committer_questionnaire.png[]

The _Committer Questionnaire_ is accessible only after an individual has been elected into the role of a committer on a project (note that all committer relationships are between an individual and a specific project), either as an initial committer on a new project, or via <<elections-committer,_Committer Election_>> on an existing project. New committers will be sent a link to the questionnaire via email.

[NOTE]
====
If you indicate that you are employed, you will be asked to identify your employer. Only member companies that have provided a *signed* _MCCA_ will be listed as member companies in the _Committer Questionnaire_. If you work for a company that is not an Eclipse Foundation Member Company, or your employer has not provided the Eclipse Foundation with a _MCCA_ you must list your employer as "Other".
====

[[paperwork-documents]]
=== Documents

The specific agreements required depends on the nature of the project and the employment status of the individual. For committers on an open source _software_ project (i.e., most {forgeName} projects), the agreements described below are required. 
For committers on an open source _specification_ project, additional <<specifications-agreements,working group agreements>> are required.

[graphviz, images/paperwork, svg]
.What agreements are required?
----
digraph {
	// Graph properties
	bgcolor=transparent
	
    graph [ranksep="0.25", nodesep="0.25"]
    
	// Nodes that define the key points in the process
	node [shape=box;style=filled;fillcolor=white;fontsize=12;group=g1]
	
	start [label="Start"]
	ica [label="Submit an ICA"]
	final [label="You're done!"]
	
	node [shape=diamond;style=filled;fillcolor=white;fontsize=10;group=g1];
	
	is_committer [label="Are you already\na committer?"]
	is_member [label="Do you work for\na member company?"]
	has_mcca [label="Has your company\nprovided an MCCA?"]
	
	start -> is_committer
	
	is_committer -> final [label="Yes"]
	is_committer -> is_member [label="No"]
	
	is_member -> has_mcca [label="Yes"]
	is_member -> ica [label="No"]
		
	has_mcca -> final [label="Yes"]
	has_mcca -> ica [label="No"]
	
	ica -> final
}
----

The new committer must download and print the documents, complete and sign them, and then provide them to the EMO Records Team. The Committer Agreement Workflow provides a means of uploading these documents; alternatively, the team may ask you to provide them via email. 

.Upload the ICA directly into the form
image::images/committer_questionnaire2.png[]

After all required documents have been received, the EMO Records Team will review them, contact the committer to resolve any issues that need to be addressed, and send a request to the Eclipse IT Team to provision a committer account.

Questions regarding this process should be addressed to the mailto:{emoRecordsEmail}[EMO Records Team].

[[paperwork-mcca]]
==== Member Committer and Contributor Agreement[[paperwork-mca]]

The {mccaUrl}[Member Committer and Contributor Agreement] (MCCA) is used by organizations that are members of the Eclipse Foundation to cover all of their employees who participate in Eclipse Foundation projects as committers. 

[NOTE]
====
If your employer is a member of the Eclipse Foundation and  has not already provided an MCCA, consult with your management team to determine who has the necessary authority to sign it on your company's behalf. Provide the MCCA in advance of the completion of your committer election or new project creation to streamline the committer provisioning process. If you and your management team are not sure whether or not your employer has an MCCA, ask the mailto:{emoRecordsEmail}[EMO Records Team].
====

If the committer's employer is a member of the Eclipse Foundation that cannot provide a signed MCCA, then the committer will have to complete an _Individual Committer Agreement_.

[[paperwork-ica]]
==== Individual Committer Agreement

The {icaUrl}[Individual Committer Agreement] (ICA) is used by committers who are not covered by a {mccaUrl}[MCCA].

An ICA is required when:

* The committer works for a company that is not a member of the Eclipse Foundation;
* The committer works for member company that has not signed a MCCA;
* The committer is self employed or not employed; or
* The committer is a student.

[[paperwork-existing]]
==== Existing Committer

If the individual is already a committer on an existing Eclipse Foundation project then additional agreements may or may not be necessary. The EMO Records Team will ask for additional documentation if required.

[[paperwork-not-employed]]
==== Not Employed or Student

If the committer is not employed or is a student, they must send a note to the mailto:{emoRecordsEmail}[EMO Records Team] explaining their status.

[[paperwork-faq]]
=== Frequently Asked Questions

[qanda]

What is the difference between an Individual Committer Agreement, the Member Committer and Contributor Agreement, and the Eclipse Contributor Agreement? ::

The <<paperwork-ica, Individual Committer Agreement>> (ICA) is for committers whose employers are either not {memberUrl}[members] of the Eclipse Foundation or are members that have not signed the Member Committer and Contributor Agreement.
+
The <<paperwork-mcca, Member Committer and Contributor Agreement>> (MCCA) is used by organizations that are members of the Eclipse Foundation to cover all of their employees who participate in Eclipse Foundation open source projects as committers.
+
The <<contributing-eca, Eclipse Contributor Agreement>> (ECA) is for everybody else. The ECA covers contributors who are not otherwise covered for their contributions by an ICA or MCCA.

What happens if I do not fill out the agreements?::

If you do not fill out the agreements, then you do not get your credentials for write-access to the source code repository(s) and other project resources.

Where can I get help to discuss these documents with my management team? ::

The EMO can talk to your management and senior executives about these (and other) legal documents to help them understand why these documents are the best risk reduction solution for everyone involved. Contact us at {legalEmail}.

What formats can be used to submit paper documentation? ::

The EMO Records Team prefers a scanned document delivered via email. The documents may also be provided by fax or by post (the fax number and postal address are captured in the documents).

What Email address should I use to send completed documents? ::	

{emoRecordsEmail}

What if a I change employers? ::

Edit your {accountUrl}[Eclipse Account Profile] to update your employment status.
	