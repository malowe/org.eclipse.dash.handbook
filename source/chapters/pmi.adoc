////
 * Copyright (C) 2015,2022 Eclipse Foundation, Inc. and others. 
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-FileType: SOURCE
 *
 * SPDX-FileCopyrightText: 2015,2022 Eclipse Foundation, Inc.
 * SPDX-FileCopyrightText: 2015 Contributors to the Eclipse Foundation
 *
 * SPDX-License-Identifier: EPL-2.0
////

[[pmi]]
== Project Management Infrastructure (PMI)

The {forgeName} Project Management Infrastructure (PMI) consolidates project management activities into a single consistent location and experience.

Project Management Infrastructure themes:

_Improved consistency._ Configuration/data-driven project web presence, direct linkage between releases, reviews, and plans. Information -- including basic project metadata, project plans, and review information -- is captured and retained in a consistent (and easily leveraged) data-based format (rather than in multiple documents in arbitrary formats).

_All-in-one-place._ Project leads and committers are able to edit information in place on the project information pages. Text/information in one place with links in another is eliminated where possible. Comments and discussion related to reviews, elections, etc. are connected directly to the item being discussed.

_Get started fast._ By default, projects are provided with a data-driven website (a so-called _PMI Page_) that includes consistent links to project releases, reviews, downloads, etc. With the data-driven website, setting up project presence is a matter of providing metadata. Projects can opt provide their own xref:promotion-website[custom website]. 

[[pmi-metadata]]
=== Project Metadata

Project committers and project leads are responsible for maintaining their project's metadata. This information is an important part of being an Eclipse project.

Project metadata is:

1.  Relatively static structural information such as the project description and scope, the names of the project's mailing lists and forums, pointers to source code repositories and issues, etc.
2.  Historical information such as previous release downloads, review materials, etc.
3.  Status and future looking information such as the project and milestone plans, the features scheduled for the current release, release dates, etc.

PMC members and the Eclipse Foundation staff also have the ability to make changes on behalf of a project.

[[pmi-viewing]]
=== Viewing

The complete listing of all current {projectListUrl}[{forgeName} projects] provides one starting point for viewing projects. From here, you can link directly to a <<pmi-project-page,project page>>. Navigation options are provided to help you move from one project to another.

[[pmi-project-page]]
=== Project Pages

The PMI provides a website for every project based on the <<pmi-metadata,project metadata>>. This website includes an _Overview_ page that shows basic information about the project, a _Downloads_ page that provides links to downloads, a _Developer Resources_ page that includes links to <<source-repositories,source code repositories>>, and more. 

An example project page is shown below. The _Overview_ page includes the name and description of the project, list of project licenses, a table of recent releases, and various charts and other information (note that more charts are displayed on the _Who's Involved_ page. The website also includes links to project-specific <<pmi-commands-and-tools,commands and tools>> when the user is logged in as a project committer.

.An example project page
image::images/project-page.png[]

A logged in committer can access xref:pmi-commands-and-tools[commands and tools] from the _Committer Tools_ block on the right side of the project page; they can also xref:pmi-editing[edit project metadata] by clicking the _Edit_ link at the far right of the tab list.

The URL for a project page takes the form `pass:c,a[{forgeUrl}/projects/<projectid>]` (e.g. `pass:c,a[{forgeUrl}/projects/technology.foo]`), where `pass:c,a[<projectid>]` is the qualified <<resources-identifiers,identifier>> for the project.

[[pmi-commands-and-tools]]
=== Commands and Tools

Committers have access to several committer-specific commands and tools in the _Committer Tools_ block on the right side of project pages. The selection of commands available are context sensitive; only those commands that make sense for the logged in user are shown.

[[pmi-commands-cq]]
==== Create a Contribution Questionnaire

Any project committer create <<ip-cq, Contribution Questionnaire>> (CQ). CQs are required to track the due diligence review of contributions of project content and third-party content

To create a Contribution Questionnaire:

1. Log in to the {forgeUrl}[PMI];
2. Navigate to a <<pmi-project-page,project page>>;
3. Click menu:Committer Tools[Intellectual Property > Create a Contribution Questionnaire]; and
4. Follow the workflow.

[NOTE]
====
The CQ creation process works in two parts. First you create the CQ, then (when required) you attach the source code. You can attach the source code immediately after creating the CQ (otherwise the system will email you with instructions).
====

[[pmi-commands-iplog]]
==== Submit a Project's IP Log for Review

Any project committer generate the <<ip-iplog, Intellectual Property (IP) Log>> and submit it for review by the Eclipse IP Team. An IP Log review is required, for example, for all <<release-review, release and progress reviews>>.

To generate an IP Log:

1. Log in to the {forgeUrl}[PMI];
2. Navigate to a <<pmi-project-page,project page>>;
3. Click menu:Committer Tools[Intellectual Property > Generate IP Log]; and
4. Follow the workflow.

[[pmi-commands-release]]
==== Create a New Release

A project lead or committer can create a new <<release,release>> record. This opens a dialog requesting that a date and name be specified. Both of these values can be changed later.

To generate an IP Log:

1. Log in to the {forgeUrl}[PMI];
2. Navigate to a <<pmi-project-page,project page>>;
3. Click menu:Committer Tools[Releases > Create a new release]; and
4. Follow the workflow.

[[pmi-editing]]
=== Editing Project Metadata

Committers have the ability to edit the information managed and displayed on the project page. There are several sections on the page. When you switch the page into _Edit_ mode, you will be provided with lots of help regarding the contents of each of the fields (note that the help text is currently rendered below the fields).

.Editing project metadata.
image::images/edit-project-metadata.png[]

To set the edit project metadata: 

* Log into the PMI;
* Navigate to the <<pmi-project-page,project page>>;
* Click btn:[Edit];
* Expand sections to expose the various topic areas, and edit as required; and
* Click btn:[Save].

[TIP]
====
The changes that you make to the project metadata will be reflected immediately on the project information page.
====

If you require assistance editing the project metadata, contact the {emoEmailLink}.

Some of the fields are described below.

[[pmi-description-and-scope]]
==== Description and Scope

The project description is displayed on the _Overview_ page; the scope is displayed on the _Governance_ page.

The _description_ should start with a concise paragraph of three to five sentences (e.g. suitable for display with a collection of other projects). A single paragraph is generally appropriate for the description.

If more than a single simple paragraph is required to fully describe the project, it is possible to set a summary. The summary can be specified by toggling the menu:show summary[] link to explicitly set a summary apart from the more detailed description, or the top part of the description can be designated as the summary by inserting a _Teaser Break_ into the content.

[NOTE]
====
Providing a summary gives you control over what will get rendered. In views where we are displaying more than one project, the system will artificially cut short descriptions that are too long, potentially resulting in a description that looks _weird_.
====

The _scope_ is intended for a more select audience; generally speaking the scope should be taken directly from the project's proposal. 


[WARNING]
====
Project members have the ability to change the text of the project scope, but should be careful to **avoid changing the meaning**. If the meaning of the scope needs to change, the Project Management Committee (PMC) must be contacted regarding a potential restructuring review.
====

To edit the description and/or scope:

* Log into the PMI;
* Navigate to the <<pmi-project-page,project page>>;
* Click btn:[Edit];
* Open the section labeled _The Basics_;
* Update the fields as required; and
* Click btn:[Save].

[[pmi-downloads]]
==== Downloads

You can provide download information for your project in the _Downloads_ section.

The first entry is the main "Downloads URL". This manifests as a _Big Button_ Download on the project page. What you put here is left to the project team to decide. It can be a link to a website, a direct link to a file download, or whatever else makes sense the project and community.

Optional text can be included along with the _Big Button_ Download, as well as links to zero or more Eclipse Marketplace, update/p2 sites, or other downloads. Each of the links can have an optional title (the link itself will be displayed if no title is provided). Note that no validation is done on the links to ensure that they are meaningful. 

To edit the download information:

* Log into the PMI;
* Navigate to the <<pmi-project-page,project page>>;
* Click btn:[Edit];
* Open the section labeled _Downloads, Software Repositories, and Marketplace_;
* Update the fields as required; and
* Click btn:[Save].

[[pmi-contributors]]
==== Contributors

The project team can choose to designate specific individuals as official contributors. On xref:resources-gitlab[GitLab], designated contributors are granted the _Reporter_ role in the project's repositories. On xref:resources-github[GitHub], designated contributors are granted the _triage_ permission. Contributors can create merge and pull requests regardless of whether or not they are designated as an official contributor.

Contributors can be designed by adding their email address to the _Contributors_ field. The contributor must have an Eclipse Foundation account using the email. In the case of GitHub, the contributor must also provide their GitHub ID in their <<contributing-account,Eclipse Foundation Account>>.

[NOTE]
====
To gain the _triage_ role on {aForgeName} project's GitHub repositories, contributors must provide their GitHub ID in their xref:contributing-account[Eclipse Foundation Account].
====

Any committer or project lead can add or remove contributors for their own project. To set the add or remove contributors: 

* Log into the PMI;
* Navigate to the <<pmi-project-page,project page>>;
* Click btn:[Edit];
* Open the section labeled _Source Code and Issues/Bugzilla_;
* Add/remove email addresses to/from the _Contributors_ field;
* Click btn:[Save].

[NOTE]
====
The _sync script_ that propagates contributor information into teams on our GitLab instance or to GitHub runs every two hours. That is, you should expect upwards of two hours of latency between when an individual is added to as a contributor and the corresponding privileges are available to that individual.
====

[[source-repositories]]
==== Source Repositories

Source repositories are listed on the _Developer Resources_ page.

Metadata regarding source repositories is maintained by EMO staff.

A _Contribution Message_, which is -- when provided -- displayed at the top of the _Developer Resources_ page. Arbitrary text is permitted, but we recommend that you limit this content to a single paragraph with a few sentences that include a link to more information. 

[TIP]
====
It is more likely that potential contributors will find your project's source code repositories before they find the project page; be sure to provide a xref:legaldoc-contributor[Contributor Guide] in the root of your project repositories.
====

To edit information about contribution and provide pointers to information about contributing to your project:

* Log into the PMI;
* Navigate to the <<pmi-project-page,project page>>;
* Click btn:[Edit];
* Open the section labeled _Source Code and Issues/Bugzilla_;
* Update the fields as required;
* Click btn:[Save].

[[pmi-project-logos]]
==== Project Logos

Project Logos appear in the right navigation area on the project page. They also appear on summary or list pages (for example, the {projectListUrl}[Project List Page]).

Any member of the project team can set the project's logo. The logo will be resized to a maximum of 200x200 pixels. Project logos are considered trademarks of the Eclipse Foundation (see xref:promoting-project-logo[Logos and Graphics] for more information).

[TIP]
====
The Eclipse Foundation will use your logo to promote your project. Be sure to create your project logo with a transparent background so that it will display well on arbitrary background colors.
====

Any committer or project lead can set the logo for their own project. To set the project logo: 

* Log into the PMI;
* Navigate to the <<pmi-project-page,project page>>;
* Click btn:[Edit];
* Open the section labeled _The Basics_;
* Upload your logo to the _Logo_ field;
* Click btn:[Save].

[NOTE]
====
On a xref:pmi-project-page[project page], the PMI displays either the project logo or -- when the project is in the xref:starting-project-phases[incubation phase] -- the xref:starting-incubation-branding[incubation logo]. 

To exit from the incubation phase into a mature project, a project must meet all of the requirements and engage in a xref:release-graduation[graduation review].
====

[[pmi-company-logos]]
==== Company Logos

Company logos automatically appear on the _Overview_ and _Who's Involved_ page under the following conditions:

* The company must be a {memberUrl}[member] of the Eclipse Foundation;
* The company needs to have their logo uploaded to the Portal;
* At least one committer has to be listed as an employee of the company in question;
* The committer must be on this project; and
* The committer must be active (must have made at least one commit in the last three months)

[TIP]
====
The display of company logos is _entirely automatic_ based on the criteria listed above.
====

If all of those conditions are met and the logo is still not showing up, then it's possible that the project meta-data doesn't have the correct source code repository information specified. Contact the {emoEmailLink} for assistance.

[[pmi-build-technology]]
==== Build Technology

Build information appears on the _Developer Resources_ page.

A project can specify a section of text, links, and a selection of the build technologies employed. Specifying this information makes it easier for members from the community to understand your build. Links can include direct links into the CI builds, pages of build instructions, or whatever else the project team feels will help the community build the project.

To set build technology information: 

* Log into the PMI;
* Navigate to the <<pmi-project-page,project page>>;
* Click btn:[Edit];
* Open the section labeled _Build_;
* Upload your logo to the _Logo_ field;
* Click btn:[Save].

[[pmi-technology-types]]
==== Technology Types

Technology types are displayed in the _Tags_ block in the right navigation area of the project page.

A project can specify the types of technology produced by the project. This is specified in rather broad terms like _OSGi_ or _Runtime_. The various technology types manifest as checkboxes on the edit screen. This information is used to form connections between projects to assist in navigation and discovery.

Clicking on one of the technology types, will take the user to a page that lists the projects that produce that particular type of technology, along with the summary of their description and project logo (if specified).

To configure technology types: 

* Log into the PMI;
* Navigate to the <<pmi-project-page,project page>>;
* Click btn:[Edit];
* Open the section labeled _Categorization, Relationships, and Tags_;
* Select technology types from the list;
* Click btn:[Save].

You can also specify arbitrary tags and related projects (projects that share some link or synergy) in the _Categorization, Relationships, and Tags_ section.

[[pmi-releases]]
=== Releases and Reviews

Projects, Releases, and Reviews are presented as separate records. Each project record, obviously, represents information about a project. A project may have multiple releases; information about the release is represented in a release record. The release record also contains some review information. This information is included here, because all releases do not necessarily have a review (a project can opt to provide some _review_ type information as part of a release record. A project can have multiple review records; as release reviews are the most common form of review, most review records will be joined to a release record.

[graphviz, images/releases-and-reviews, svg]
.The relationship between proposals, projects, releases, and reviews.
----
digraph {
	bgcolor=transparent
	graph [ranksep="0.25", nodesep="0.25"];
	node [shape=box;style=filled;fillcolor=white;fontsize=12];

	review[label="Review"]
	proposal[label="Proposal"]
	project[label="Project"]
	release[label="Release"]

	proposal -> review[label="1:1"]
	project -> review[label="1:*"]
	release -> review[label="1:0..1"]
	proposal -> project[label="1:1"]
	project -> release[label="1:*"]

	{rank=same;proposal project release}
}
----

A review record, however, does not require a release association. Some reviews are associated with proposals. Other have no other association (e.g. termination reviews).

Each <<release,release>> has its own record in the database. Records are connected directly to a single specific project; a subset of release records associated with a project are displayed on the project page. An existing release can be edited in much the same was as a project. Any logged in project member (committer or project lead) can switch to the _Edit_ tab.

Create a single record for each release. **Do not create release records for xref:release-milestones[milestone builds]**. You may optionally enter milestone information in the xref:pmi-release-plan[Plan] information of a release record.

[TIP]
====
Use the <<pmi-commands-release, Create a new release>> command on a specific <<pmi-project-page, project page>> in the <<pmi,Project Management Interface (PMI)>> to create a new release record.
==== 

[[pmi-release-description]]
==== Description

Describe the release in the _Description_ section. The description should generally be a concise paragraph describing the focus of the release (e.g. adding certain functionality, fixing bugs, etc.) in a form that is appropriate in an aggregation (e.g. a page that displays the release information for all projects participating in an instance of the Simultaneous release). The description should provide enough information to encourage the reader to click the menu:find out more[] link.

[[pmi-release-issues]]
==== Issues

The _Issues_ tab is populated with descriptions and links to issues in the Eclipse Foundation's Bugzilla instance that are associated with the release. This feature has been flagged for deprecation and removal (see https://gitlab.eclipse.org/eclipsefdn/it/websites/projects.eclipse.org/-/issues/83[issue #83] for more information).

The release record will automatically generate a list of targeted bugs.

To populate this list:

* Ensure that the Bugzilla _Product_ is set the to correct value in the project metadata;
* Set the _target milestones_ in Bugzilla need to match the name of your release.

[NOTE]
====
The matching algorithm tries to be as forgiving as possible, a release named `3.5`, `3.5.0`, or `3.5 (Luna)` will -- for example -- match target milestones named `3.5` ,`3.5M1`, `3.5 M2`, `3.5.0M3`, etc., but will not match `3.5.1`.
====

The bugs for all projects participating in the release will be included. Bugs are grouped by Bugzilla product and component.

[[pmi-release-plan]]
==== Plan

<<releases-plan,Project plan>> information belongs in the _Plan_ section. This information *should* generally be provided early in the development cycle to allow the various communities the ability to understand and participate in the release. It is expected that the plan will evolve over time. Note that all projects are required to provide a plan for each major and minor release (plans are not required service releases).

[[pmi-review]]
==== Review

The release record has a <<release-review,_Review_>> section that can be used to provide information for the associated review. If you provide information in the _Review_ section, the release record itself can be used as review documentation; no further documentation is required.

Each section on the review page includes a little help to describe the sort of information that you should provide.

[NOTE]
====
A review is not necessarily required for every release. A project team may declare official major or minor releases and distribute associated products for up to one year following a successful _<<release-review,release or progress review>>_ and reviews are never required for bug-fix/service releases. 
====

If a release requires a review, you can schedule one by clicking the menu:Schedule a review[] link. The drop-down list above the button contains  several options for review dates. Pick the one that works best for you.

Note that this form will not appear if a review has already been scheduled, or the release date does not provide enough time to run a review (or is in the past). If a review has been scheduled, a link to the review will appear.

You can edit the review document, but there's really not all that much to edit. A free-form text field is available and can be used if there is some need to provide review-specific information that might not otherwise be an appropriate part of the release record. 

[NOTE]
====
The free-form text field in the review record is intended for other types of reviews (e.g. restructuring or termination reviews); we decided to leave it available for release reviews for cases in which it might be useful rather than suppress it.
====

When the review is displayed, it automatically includes the _review_ information from the release record; it shows the review-specific information at the top of the page, and includes the _review_ information from the release as the rest of the page contents.

This can make things a bit confusing when you want to make changes to the metadata for a review record. Just remember that the _review_ information for a release is stored in the release record.

[[pmi-joining-a-simultaneous-release]]
=== Joining a Simultaneous Release

A simultaneous release (also referred to as a _coordinated release_ or _release train_) is a means of grouping together projects that are coordinating their respective releases and need a means of representing the aggregation. Simultaneous releases must themselves coordinated and managed by supervising body (e.g., a PMC or working group specification committee). The rules for joining and participating in a simultaneous release are defined by the supervising body.

[NOTE]
====
Simultaneous releases are not a formal concept in the EDP. While the supervising body has considerably latitude to define the nature of their simultaneous release, the project teams that opt to participate in a simultaneous release are still individually required to fully meet their obligations under the EDP and the Eclipse IP Policy.
====

To create a simultaneous release record and update its contents, the supervising body must connect with the {emoEmailLink}.

[TIP]
====
The Eclipse Planning Council manages the https://wiki.eclipse.org/Category:SimRel[Eclipse IDE Simultaneous Release], and maintains the https://wiki.eclipse.org/SimRel/Simultaneous_Release_Requirements[participation rules] (in particular, see the https://wiki.eclipse.org/SimRel/Simultaneous_Release_Requirements#State_intent_early[process for joining]). 
====

[TIP]
====
The https://jakarta.ee/committees/specification/[Jakarta EE Specification Committee] manages the Jakarta EE release.
====

To join a simultaneous release, {aForgeName} project committer must

1. Meet the requirements defined by the supervising body;
2. Create a <<pmi-releases,release record>>:
* The record must include at least brief description of the release (that can be changed later); and
* The date of the release should generally match that of the simultaneous release;
3. The committer must then connect with the supervising body and the {emoEmailLink} to add their release record to the simultaneous release record.

[NOTE]
====
Release records are added to a simultaneous release record by reference. If you change information in the release (including the release name/number), those changes will be automatically reflected.
====

[[pmi-faq]]
=== Frequently Asked Questions

[qanda]

Why doesn't a committer appear on the project's _Who's Involved_ page? ::

For a committer to appear on a project's _Who's Involved_ page:
+
--
. The new committer must be <<elections-committer,elected>> (or be listed as an initial committer on a <<starting-proposal,project proposal>>);
. The election must complete successfully and be approved by the PMC;
. The new committer must provide the required <<paperwork,paperwork>>;
. The paperwork must be processed by the EMO Records Team; and
. The Eclipse IT Team must create the committer account.
--
+
Each piece takes time. If a committer doesn't appear some number of days after the successful completion of a committer election, then contact the committer to confirm that they've followed the provided instructions and have submitted committer paperwork. Then, connect with mailto:{emoEmail}[EMO] for assistance.
+
If the new committer will be working with projects that use GitHub, they must be sure to add their GitHub Id to their <<contributing-account,Eclipse Foundation Account>> so that our scripts can add them to the GitHub Teams that we use to manage access to Eclipse Foundation repositories hosted on GitHub.

What is the difference between a _Committer_ and an _Historic Committer_? ::

The _Who's Involved_ page lists committers and (in many cases) historic committers. Committers are those individuals who currently hold committer status (i.e., they can write to project resources). Historic committers are retired former committers. While they hold a special place in our hearts, they have no official status on the project. To turn an historic committer back into a committer, they must be <<elections-committer,re-elected>> to the project.