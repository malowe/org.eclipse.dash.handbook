////
 * Copyright (C) 2015 Eclipse Foundation, Inc. and others. 
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-FileType: SOURCE
 *
 * SPDX-FileCopyrightText: 2015 Eclipse Foundation, Inc.
 * SPDX-FileCopyrightText: 2015 Contributors to the Eclipse Foundation
 *
 * SPDX-License-Identifier: EPL-2.0
////

[[contact]]
== Getting Help

If you have any questions, or are unsure of your responsibilities as a project lead or committer, please contact your project mentors or {emoEmail}.

To report vulnerabilities or report security issues contact {securityTeamEmail}. Please see <<vulnerability, Managing and Reporting Vulnerabilities>>.

To ask questions about licensing or issues of a legal nature, contact {legalEmail}.

If you're not sure, contact {emoEmail}.